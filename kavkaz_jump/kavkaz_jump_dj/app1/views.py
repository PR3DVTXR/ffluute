from django.shortcuts import render



def home(request):
    return render(request, 'app1/home.html')

def blog(request):
    return render(request, 'app1/blogs.html')